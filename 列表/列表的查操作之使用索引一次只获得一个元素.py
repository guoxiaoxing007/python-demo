"""列表的查操作之使用索引一次只获得一个元素"""

"""
    使用索引获得列表中的元素，一次只获得一个元素。
"""
L = [5, 3, 9, 4, 0, 6, 8, 1, 7, 2]
print(L[0])     # 5
print(L[-10])   # 5

"""
    若指定索引在列表中不存在，会抛出IndexError。
"""
# print(L[10])    # IndexError: list index out of range
