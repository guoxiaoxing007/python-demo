"""列表的查操作之使用运算符in检查是否存在指定元素"""

"""
    使用运算符in（not in）检查列表是否存在（不存在）指定元素
    in，存在返回True，不存在返回False
    not in，不存在返回True，存在返回False

"""
L = [3, 4, 5, 6, 7]
print(5 in L)
print(8 in L)

print(5 not in L)
print(8 not in L)
