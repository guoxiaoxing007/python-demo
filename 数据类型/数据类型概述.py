""" 数据类型概述"""

"""
一、数据类型
    对数据的分类：整数类型、浮点类型、字符串类型等等
"""

"""
二、获取数据类型
    内置函数 type()
"""
print(type(18))     # <class 'int'>
print(type(5.6))    # <class 'float'>
print(type("Hello"))    # <class 'str'>