"""布尔类型"""

"""
一、布尔类型
    只有两种取值：True、Flase
"""
print(5 > 3)  # True
print(5 < 3)  # False

"""
二、True的值为1，False的值为0
"""
print(True == 1)        # True
print(False == 0)       # True
print(True + False + 5)     # 6
