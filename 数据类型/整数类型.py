""" 整数类型 """

"""
一、整数有4种进制表示方式
（1）10进制：默认
（2）2进制：0b开头
（3）8进制：0o开头
（4）16进制：0x开头
"""
print(118)
print(0b1110110)
print(0o166)
print(0x76)

"""
二、整数转换为不同进制的字符串
    可以调用内置函数将十进制整数转换为不同进制的字符串
1.bin()：十进制整数转为2进制（binary）字符串
2.oct()：十进制整数转为8进制（octal）字符串
3.hex()：十进制整数转为16进制（hexadecimal）字符串
"""
print(bin(118))     # 0b1110110
print(oct(118))     # 0o166
print(hex(118))     # 0x76

"""
三、整数的创建
    1、直接创建
    2、调用内置函数int创建
    不传任何参数，返回整数0
    只传一个参数，将参数转换为整数
    传递两个参数，第一个参数必须是字符串，第二个参数指定进制
    
"""
print(int())    # 0

print(int(118))     # 118
print(int(118.2))   # 118
print(int('118'))   # 118

print(int('1110110',2))     # 118
print(int('0o166', 8))      # 118
print(int('0x76',16))       # 118