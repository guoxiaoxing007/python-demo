"""序列类型range"""

"""
    range，序列类型，表示不可变的整数序列。
    调用内置函数range（类range的构造方法），创建range类型的对象，有三种调用方式：
    1.range(stop)
    2.range(start, stop)
    3.range(start, stop, step)
    起始值start,默认为0
    结束值stop，不包含stop
    步长step，默认为1
    
    内置函数range的返回值是一个迭代器对象，为清除表示返回的迭代器对象所表示的整数序列，可将其转换成列表。
    
    优点：不管range对象表示的整数序列有多长，所有range对象占用的内存空间都是相同的，因为仅需存储start、stop和step。
         只有用到range对象时，才会去计算序列中的相关元素。
"""
print(range(5))  # range(0, 5)
print(list(range(5)))  # [0, 1, 2, 3, 4]
print(list(range(0, 5, 1)))  # [0, 1, 2, 3, 4]

print(list(range(1, 5, 1)))  # [1, 2, 3, 4]

print(list(range(0, -20, -4)))    # [0, -4, -8, -12, -16]


"""
    使用运算符in(not in)检查range对象表示的整数序列中是否存在（不存在）指定的整数。
"""
print(3 in range(5))    # True
print(8 not in range(5))    # True
