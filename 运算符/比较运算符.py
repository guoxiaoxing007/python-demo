"""比较运算符"""

"""
一、比较运算符
    结果是布尔值
    <  <=  > >=  !=
    == 比较两个运算数是否相等，相等性测试
    is 比较两个运算数是否是同一个对象，同一性测试
    is not
"""
print(6 < 9)
print(8 > 6)
print(8 == 8.0)
print(8 != 8.0)

a = b = [1, 2, 3]
c = [1, 2, 3]
print(a == b)  # True
print(a == c)  # True
print(a is b)  # True
print(a is c)  # False

"""
二、不可变类型对象的is比较
    对于不可变类型的对象，其内存可能会被重用，如数值较小的整数对象
    可调用内置函数id进行验证，返回的是对象的唯一标识，即对象在内存中的地址。
"""
a = 18
b = 18
print(id(a))  # 4457675600
print(id(b))  # 4457675600
print(a is b)  # True

"""
三、可以链式比较
"""
age = 18
print(0 < age < 100)  # True

print(1 == 2 < 3)   # print(1 == 2 and 2 < 3)   False
