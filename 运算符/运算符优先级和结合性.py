"""运算符优先级和结合性"""

"""
一、运算符优先级
    固定优先级，高优先级先参与运算。
"""
# *优先级比+高，先参与运算
print(2 + 3 * 4)

"""
二、运算符结合性
    固定结合性，优先级相同时，结合性定义了谁先参与运算。
    结合性为左，左边运算符先参与运算。 eg:2 + 3 - 4
    结合性为右，右边运算符先参与运算。 eg:a = b = 18
"""

"""
三、正确使用运算符的优先级和结合性
    没必要记忆所有的。
    包含多个运算符的复杂表达式，建议做法：
    1.使用小括号指定运算顺序。
    2.拆分成几步来完成。
"""
is_has_key = False
is_entered_door = False
is_passed_scan = False
is_know_password = True

# and比or优先级高，or的结合性是左
print(is_has_key or is_entered_door and is_passed_scan or is_know_password)

# 使用小括号指定运算顺序
print((is_has_key or (is_entered_door and is_passed_scan)) or is_know_password)

# 拆分成几步来完成
step1 = is_entered_door and is_passed_scan
step2 = is_has_key or step1
step3 = step2 or is_know_password
print(step3)

