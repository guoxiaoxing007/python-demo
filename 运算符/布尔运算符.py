"""布尔运算符"""

"""
一、布尔运算符  与 或 非
    and：全真为真，有假为假
    or：有真为真，全假为假
    not：取反  为真则假，为假则真
"""
print(True and True)
print(True and False)

print(True or False)
print(False or False)

print(not True)
print(not False)