"""关键字和标识符"""

"""
一、关键字
    Python定义的具有特殊用途的单词。
    
    1.通过内置函数help()查看所有关键字：
    >>> help()
    Welcome to Python 3.9's help utility!
    ....
    help> keywords
    
    2.通过导入模块keyword查看所有关键字：
    >>> import keyword
    >>> keyword.kwlist
    
    False               break               for                 not
    None                class               from                or
    True                continue            global              pass
    __peg_parser__      def                 if                  raise
    and                 del                 import              return
    as                  elif                in                  try
    assert              else                is                  while
    async               except              lambda              with
    await               finally             nonlocal            yield
"""

"""
二、标识符
    给程序中变量、函数、方法、类等命名的名字。
    
    命名规则：
    1.区分大小写
    2.不能是关键字
    3.不能以数字开头
    4.不能包含空格、制表符、数学符号、中划线、箭头等
    
    命名规范：
    1.见名知意，由一个或多个有意义的单词组合而成。
    2.所有单词全部小写，单词之间用下划线分隔。
"""
# 标识符区分大小写
i = 3
I = 5
print(i)  # 3
print(I)  # 5

# Assignment to keyword 标识符不能是关键字
# True = 8

# End of statement expected 标识符不能以数字开头
# 5i = 18

# End of statement expected 标识符不能包含空格、制表符、数学符号、中划线、箭头等
# i 5 = 18
