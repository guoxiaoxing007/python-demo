"""对象None"""

"""
一、对象None
    表示数据值不存在。
    占据一定内存空间，不意味着“空”或“未定义”。
    None是something，不是nothing。
"""
print(id(None))     # 4384374496

"""
二、使用场景
    1.变量初始化
    2.将变量重置为“数据值不存在”的状态
"""
a = None
print(a)    # None

b = 18
print(b)    # 18
b = None
print(b)    # None